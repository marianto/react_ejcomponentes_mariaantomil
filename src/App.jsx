import React from 'react';
import './App.css';
import HelloWorld from './components/helloWorld';
import HelloWorldProps from './components/HelloWorldProps';
import List from './components/List';
import { Counter } from "./components/Counter";
import UserInfo from './components/UserInfo';
import UserList from './components/UserList';
import UserListSuper from './components/UserListSuper';
import UserForm from './components/UserForm';

const name = 'Pepita Flores';

function App() {
  return (
  <div className="App">
      <div className="App-header">
        <HelloWorld/>
        <List/>
        <Counter/>
        <HelloWorldProps name={name} age={25} text={"helloooooooooo"}/>
        <UserInfo userProd={{
          name:'Maria Antomil',
          years: 29,
          rol: 'pintora'
        }}/>
        <UserList/>
        <UserListSuper></UserListSuper>
        <UserForm/>

    </div>
  </div>
  );
}

export default App;
