import React, { Component } from 'react';

export class Counter extends Component {
    state = {counter: 0};

    plusOne = () => {
        this.setState({ counter: this.state.counter + 1 });
        // this.state = { counter: this.state.counter + 1 }; MAL

        setTimeout(() => {
            console.log('##ABEL## >> Counter >>  plusOne', this.state.counter);
        }, 0)
        console.log('##ABEL## >> Counter >>  plusOne', this.state.counter);
    }

    minusOne = () => {
        this.setState({ counter: this.state.counter - this.getNumberValue() });
    }

    multiOne = () => {
    this.setState({ counter: this.state.counter * this.getNumberValue() });
    }

    divOne = () => {
        this.setState({ counter: this.state.counter / this.getNumberValue() });
    }

    reset = () => {
        this.setState({ counter: this.state.counter = 0 });
    }

    getNumberValue = () =>  {
        return Number(document.getElementById('numberToUse').value);
    }


    render () {
        return (
            <div>
              
                {/*<button onClick={this.plusOne.bind(this)}>+</button>*/}
                <button onClick={this.plusOne}>+</button>
                
                <div>
                <button onClick={this.minusOne}>-</button>
                </div>
                <div>
                <button onClick={this.multiOne}>x</button>
                </div>
                <button onClick={this.divOne}>/</button>
                <button onClick={this.reset}>C</button>
                <p>
                    {this.state.counter}
                </p>
                <input type="number" id="numberToUse" />
            </div>
        )
    }
}
