import React , {Component} from 'react';
export default class HelloWorldProps extends Component {
    render () {
        return (
            <div>
            <p>{this.props.text} {this.props.name} {this.props.age}</p>
            </div>
            )
    }
}