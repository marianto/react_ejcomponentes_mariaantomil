import React , {Component} from 'react';
export default class List extends Component {
    render () {

        let items = [];
        for (let i = 0; i<= 5; i++) {
            items.push(<li key={i}>Valor {i}</li>);
        }
        return (
            <ul>
                {items}
            </ul>
            )
    }
}

//Cuando hacemos un for/map y pusheamos n veces un elemento React exige el elemento key porque react necesita un identificador para cada uno de los elementos para optimizar el procesado//
