import React, {Component} from 'react';

const defaultUser = {
    name: 'Guillermo Cascarabias ',
    years: 0,
    rol: 'Cantante '
};

export default class UserForm extends Component {

    state = defaultUser;

    //cuando en mi formulario cambio un valor en mi imput se produce el evento handleChange//
    //el evento Onchange tiene el target que te indica el elemento del imput donde ha ocurrido el evento y el .value recoge el valor//
    handleChange = ($event) => {
        this.setState( {[$event.target.name]: $event.target.value});
    }

    //$event es el paramatro por defecto del evento onSubmit que está en la 1º posicion de cuando se ejecuta el evento Onsubmit//
    saveUser = ($event) => {
        $event.preventDefault();
        this.props.fnAddUser(this.state);
        this.setState(defaultUser)
    }



render() {
    return (

        <form onSubmit={this.saveUser}>
             <input type="text" value={this.state.name} name = 'name' onChange={this.handleChange}/>
             <input type="number" value={this.state.years} name = 'years' onChange={this.handleChange}/>
             <input type="text" value={this.state.rol} name = 'rol' onChange={this.handleChange}/>
             <input type="submit" value="Guardar"/>

        </form>


    )
}



}
