import React, { Component } from 'react';

export default class UserInfo extends Component {
    render () {
        const userProd = this.props.userProd;
        return (
            
            <div>
            <p>Me llamo{userProd.name},
            tengo{userProd.years},
            me dedico a {userProd.rol}</p>
            {/*le envio el output a traves de las propiedades por eso hago this.props.fnRemoverUser que la ejecuto con los parentesis */}
            <button onClick={() => {this.props.fnRemoveUser(this.props.index)}}>x</button>
            </div>
            )
    }
}