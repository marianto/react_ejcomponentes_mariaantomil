import React, { Component } from 'react';
import UserInfo from './UserInfo';

export default class UserList extends Component {

    //declaro en el estado un array de usuarios//

    state = {

        users: [
            {
                name: 'Maria Antomil',
                years: 35,
                rol: 'teacher'
            }, {

                name: 'Carolina Tratame',
                years: 40,
                rol: 'cantante'
            }

        ]

    };

    render () {

    let prods = [];

    for (let i = 0; i < this.state.users.length; i++) {
        let user = this.state.users[i];
        //aqui estoy recogiendo el usuario//
        prods.push(<UserInfo key={i} index={i} userProd={user}></UserInfo>)
    }

    return (
        <div>
            {prods}
        </div>
    )
    }

}
