import React, { Component } from 'react';
import UserList from './UserList';
import UserInfo from './UserInfo';
import UserForm from './UserForm';

export default class UserListSuper extends Component {

    state = {

        users: [
            {
                name: 'Maria Antomil',
                years: 35,
                rol: 'teacher'
            }, {

                name: 'Carolina Tratame',
                years: 40,
                rol: 'cantante'
            }

        ]

    };


    removeUser = (index) =>{
        // alert(index);
        const users = [...this.state.users];
        users.splice(index, 1);
        this.setState({users})
        {/*actualizamos el estado con this.setState y lo modificamos con users:this.users.splice que me devuelve el array resultante menos el elemento que quiero eliminar*/}
    }

    addUser = (user) => {
        this.setState({users: [this.state.users,user]})
    }

    render () {
    let prods = [];

    // for (let i = 0; i < this.state.users.length; i++) {
    //     const user = this.state.users[i];
    //     prods.push(<UserInfo key={i} index={i} userProd={user}></UserInfo>)
    // }
        //hacer el for o hacer solo this.state.users.map es lo mismo, puedo eliminar hasta la linea 39 que me sigue ejecutando lo mismo//
        //fnRemoveUser le estoy pasando la funcion al componente hijo que la va a ejecutar en UserInfo (Componente hijo) cuando se produzca el evento onClick//
    return (
        <div>
            {/*en el array i es el index del array que me va a servir para eliminar mediante posicion usario 0 o 1*/ }
            <UserForm fnAddUser={(user) => {this.addUser(user)}}/>
            {this.state.users.map((user,i) => <UserInfo key={i} index={i} userProd={user} fnRemoveUser={this.removeUser}/>)}

        </div>
    )
}
}